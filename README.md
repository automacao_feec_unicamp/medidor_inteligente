# Sistema de Medição Inteligente

![Video](https://gitlab.com/automacao_feec_unicamp/medidor_inteligente/wikis/blob/video.mp4)

O Sistema de Medição Inteligente é uma solução para o combate de perdas não técnicas e um incentivo ao consumo racional e seguro de energia elétrica, que pode ser instalado em qualquer unidade consumidora, seja residencial, comercial ou pequenas indústrias.

Este sistema é composto por dois módulos, sendo um o Módulo Medidor Transmissor, que é responsável por medir o consumo da unidade consumidora e enviar os dados dessa medição para o segundo módulo, o Módulo Concentrador de dados que é implementado com a utilização do Raspberry PI, e realiza a leitura dos dados medidos disponibilizando-os através de um servidor Web.

A comunicação entre os dois módulos é realizada através de um sistema PLC (Power Line Communications), ou seja, o meio físico para a comunicação é a própria instalação elétrica da unidade consumidora, evitando assim a utilização de cabeamento ou de redes Wireless.
Utilizar o sistema elétrico já presente na unidade consumidora faz com que esse sistema seja de fácil instalação em qualquer edificação que possua uma rede elétrica em funcionamento conforme as normas.
A comunicação PLC é feita através dos condutores de Terra e Neutro, facilitando assim a construção de filtro e a isolação, baixando o custo do sistema.

A base de desenvolvimento para a comunicação via PLC foi um protótipo implementado por um aluno orientado pelo Prof. Elnatan C. Ferreira da FEEC.
Os módulos de comunicação foram reconstruídos, com os aperfeiçoamentos necessários para permitir que a rede unidirecional de comunicação estivesse funcional.

Com este sistema, o usuário será capaz de monitorar através da Web, e em tempo real, o consumo de sua unidade consumidora, juntamente com o nível de tensão.
O software também realiza e disponibiliza no site uma previsão do gasto mensal com o consumo de energia, utilizando os valores de custo do kWh (kiloWatt-hora) da concessionária local de energia.


Informações: [wiki](https://gitlab.com/automacao_feec_unicamp/medidor_inteligente/wikis/home)

## Autores

Stéfano Regis Gualtieri, Marllon Welter Schlischting, Marcelo Del Fiore
