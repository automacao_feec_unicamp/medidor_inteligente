/*
 * Marllon Welter Schlischting
 * 14/10/2016
 *
 */
#include <msp430fe427.h>
#include <math.h>

//Declaration of Variables
signed int Iinst[60];
signed int Vinst[60];
unsigned long long int Vacum, Iacum;
unsigned long int Vrms, Irms;
signed long long int Pacum;
float FP, S, P, Pf;
unsigned int i, c, cont, Tc, teste, F_TX;
char data;

typedef union
{
	float f;
	char sh[4];
}union_float;

union_float tensao;
union_float corrente;
union_float energia;

void init_system(void)
{
	SCFI0 |= FN_2 | FLLD_1;
	FLL_CTL0 |= XTS_FLL | DCOPLUS;
	FLL_CTL1 |= FLL_DIV_1;					//Clock Configuration - MCLK = 13MHz

	P1DIR = BIT0 | BIT1 | BIT5 | BIT2;      // P1.0, P1.1, P1.2 e P1.5 - Output
	P1SEL = BIT1 | BIT5 | BIT2;             // P1.1, P1.2 e P1.5 - MCLK, ACLK, PWM

	P2DIR = 0x10;							//P2.4 Output
	P2SEL = 0x10;							//P2.4 TX
}

#pragma vector=TIMERA1_VECTOR
__interrupt void Timer_A (void)
{
	switch(TAIV)
	 {
	   case  2: break;                          // CCR1 Interrupt not used
	   case  4: Tc++;							// CCR2 Interrupt
	   	   	    if(Tc==15)						// Tc Counts until 1s
	   	   	    {
	   	   	    	//P1OUT ^= 0x01;              // Toggle P1.0(LED Pulse) using exclusive-OR
	   	   	    	cont=1;
	   	   	    }
		   	    break;
	   case 10: break;							// Overflow Interrupt not used
	 }
}

void init_timer(void)						//Timer A configuration for energy pulse
{
	TACCTL1 = 0x00;							//Clear PWM Configurations
	TACTL |= TACLR;							//Clear Timer A Counter
	TACTL = TASSEL_1 + ID_3 + MC_1;			//TASSEL_1 -> Clock Selection: ACLK //ID_3 -> Prescaler Divide_by_8 //MC_1 -> Count to CCR0
	TACCR0 = 0xFFFF;						//Set CCR0 period -> 71ms
	TACCTL2 |= CCIE;						//Enable CCR2 Interrupt
}

void init_pwm(void)							//Timer A configuration for PWM
{
	TACTL |= TACLR;							//Clear Timer A Counter
	TACCTL2 = 0x00;							//Turn Off CCR2 Interrupt
	TACCR1 = 7;//7; 							//PWM Duty Cycle -> 50%
	TACCR0 = 14;//14; 							//PWM Period ->
	TACTL = TASSEL_2 + MC_1; 				//TASSEL_2 -> Clock Selection: SMCLK //MC_1 -> Count to CCR0
	TACCTL1 = OUTMOD_7; 					//Timer A Output Mode: RESET/SET
}

void init_analog_front_end(void)			//AD Configuration
{
  SD16CTL |= SD16SSEL_2 					// Clock selection: ACLK
          | SD16DIV_3   					// divide by 8 => ADC clock: 922,032kHz
          | SD16REFON;  					// Use internal reference

  SD16INCTL0 |= SD16INCH_0;  				// Putting Channel0 for Input
  SD16INCTL2 |= SD16INCH_0;  				// Putting CHannel2 for Input

  // Configure analog front-end channel 2 - Current
  SD16INCTL0 |= SD16GAIN_4;    							// Set gain(*4) for channel 0 (I1)
  SD16CCTL0 = SD16OSR_256 | SD16SC | SD16DF | SD16IE;  	// Set oversampling ratio to 64 (default)

  // Configure analog front-end channel 2 - Voltage
  SD16INCTL2 |= SD16GAIN_1;     						// Set gain(*1) for channel 2 (V)
  SD16CCTL2 = SD16OSR_256 | SD16SC | SD16DF | SD16IE;	// Set oversampling ratio to 64 (default)

}

void init_uart(void)
{
	UCTL0 =  (0*PENA)     		// Parity Enable         0=Disabled
	        |(0*PEV)     		// Parity Select         0= Odd
	        |(0*SPB)      		// Stop Bit Select       0= 1 Stop bit
			|(1*CHAR)      		// Character Length      1= 8-bit data
			|(0*LISTEN)   		// Loopback control      0= Loopback disabled
			|(0*SYNC)      		// Synchronous Mode      0= UART mode
			|(0*MM)      		// Multiprocessor Mode   0= Idle-line multiproc control
			|(1*SWRST);   		// Software Reset        1= Logic Held in Reset State ...
	UTCTL0 |= SSEL0;            // CLock selection UCLK -> ACLK
	UBR00 = 0x00;               // Baud Rate Selection -> 9600
	UBR10 = 0x03;               // Baud Rate Selection -> 9600
	UMCTL0 = 0x52;              // Modulation
	ME1 |= UTXE0;   			// Enable USART0 TXD
	UCTL0 = 0x10;				// SWRST = 0
}

void send_TX()							// Send Data Routine
{
	init_pwm();							// Init PWM for PLC Modulation OOK
	U0TXBUF = 0x55;						// Send Header
	while((U0TCTL & TXEPT) != 1)		// Wait for the flag TXEPT -> Transmission Complete
		{
			__no_operation();
		}
	for(c=4;c>0;c--)					// Loop to send the voltage float value
	{
		U0TXBUF = tensao.sh[(c-1)];
		while((U0TCTL & TXEPT) != 1)	// Wait for the flag TXEPT -> Transmission Complete
		{
			__no_operation();
		}
	}
	for(c=4;c>0;c--)					// Loop to send the current float value
	{
		U0TXBUF = corrente.sh[(c-1)];
		while((U0TCTL & TXEPT) != 1)	// Wait for the flag TXEPT -> Transmission Complete
		{
			__no_operation();
		}
	}
	for(c=4;c>0;c--)					// Loop to send the energy float value
	{
		U0TXBUF = energia.sh[(c-1)];
		while((U0TCTL & TXEPT) != 1)	// Wait for the flag TXEPT -> Transmission Complete
		{
			__no_operation();
		}
	}
	U0TXBUF = 0xAA;						//Send CRC
	while((U0TCTL & TXEPT) != 1)		// Wait for the flag TXEPT -> Transmission Complete
		{
			__no_operation();
		}
}

#pragma vector=SD16_VECTOR				// AD Interruption
__interrupt void SD16ISR(void)
{
	switch (SD16IV)
	  {
	  case 2:                           // SD16MEM Overflow Interrupt not used
		  break;
	  case 4:                           // SD16MEM0 Channel 0 Interrupt
		  Iinst[i]=SD16MEM0;			// Save the Current Instant Value
		  break;
	  case 6:                           // SD16MEM1 Channel 1 Interrupt not used
		  break;
	  case 8:                           // SD16MEM2 Channel 2 Interrupt
		  Vinst[i]=SD16MEM2;			// Save the Voltage Instant Value
		  i++;							// Increment Counter for 60 samples
		  break;
	  }

}

//------------------------------------------------------------------------------------------------------

int main(void)
{
	WDTCTL = WDTPW | WDTHOLD;			// Stop watchdog timer
	init_system();						// Init Clocks and Ports
	init_analog_front_end();			// Init and Configure AD
	init_uart();						// Init UART Communication
	init_timer();						// set time for energy calc
	__enable_interrupt();       		// Enable general interrupts

	while(1)							//Infinit Loop
	{
		if(i==60)						//If all the 60 samples have been collected
		{
			SD16CCTL0 = 0x32;           // Disable AD interrupt for Channel 0
			SD16CCTL2 = 0x32;			// Disable AD interrupt for Channel 2
			for(c=0;c<60;c++)
			{
				Iinst[c] -= -26;		// Eliminate Off-set from the current samples
				Vinst[c] -= 10;			// Eliminate Off-set from the voltage samples
			}
			for(c=0;c<60;c++)								//Loop for Calculations
			{
				Iacum += ((long int)Iinst[c]*Iinst[c]);		//Current Accumulator for RMS Calculation
				Vacum += ((long int)Vinst[c]*Vinst[c]);		//Voltage Accumulator for RMS Calculation
				Pacum += ((signed long long int)Vinst[c]*Iinst[c]);		//Active Power Accumulator for Energy Calculation
			}
			Irms=sqrt((float)Iacum/i);				//Calculating the Current RMS value
			Vrms=sqrt((float)Vacum/i);				//Calculating the voltage RMS value
			P=(Pacum/i);							//Calculating the Active Power value
			tensao.f=Vrms*0.0082517006;				//Calculating the voltage real value //Calibration constant -> 0.0083736559
			corrente.f=Irms*0.0005524629;			//Calculating the current real value //Calibration constant -> 0.0005524629
			S=tensao.f*corrente.f;					//Calculating Apparent Power
			Pf=P*0.0082517006*0.0005524629;			//Calculating the Active power real value in 1s
			FP=Pf/S;								//Calculating the Power Factor
			energia.f=Pf/3600;					//Calculating energy in Wh
			i=0;									//Clear i counter
			Iacum=0;								//Clear Accumulators
			Vacum=0;
			Pacum=0;
		}
		if(cont==1)									//When 1s have been passed
		{

			P1OUT = 0x01;
			send_TX();								//Send calculated values
			cont=0;									//Reset Energy pulse timer counter
			Pacum=0;
			Tc=0;									//Reset Energy pulse Timer counter
			init_timer();							//Restart Timer A and Shutdown PWM
			P1OUT = 0x00;
			SD16CCTL0 = 0x3A;						//Enable AD interrupt for Channel 0
			SD16CCTL2 = 0x3A;                       // Enable AD interrupt for Channel 2

		}
	}
}
