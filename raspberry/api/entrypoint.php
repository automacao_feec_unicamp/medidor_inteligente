<?php 

/**
 * \brief SMI RestAPI
 * 
 * Entrada principal da interface web service SMI.
 * 
 * Toda requisi��o WebService possui a estrutura m�nima:
 * 	'mod'	(WS_MOD)	:	m�dulo de acesso
 * 	'func'	(WS_FUNC)	:	fun��o a ser executado dentro do m�dulo
 * 
 * Para fun��es que requerem parametros, estes devem ser enviados na sequencia contendo
 * a identifica��o do parametro e valor no formato: "parametro"="valor", exemplo, para ler medicoes  
 * o HTTP POST deve ser "mod=smi&func=getmetering&metaddr=1" (VER 'smi/smi_defs.php')
 * 
 * Toda resposta do WebService segue a estrutura:
 * 	'mod'		(WS_MOD)				:	identifica��o do m�dulo processado
 * 	'func'		(WS_FUNC)				:	identicica��o da fun��o processada
 * 	'success'	(WS_RESPONSE_CODE)			:	c�digo de retorno 0 (WS_RESPONSE_ERROR) se Erro ou 1 (WS_RESPONSE_SUCCESS) se fun��o processada
 * 	'response'	(WS_RESPONSE_MESSAGE)		:	array composto obrigatoriamente por 2 partes 
 * 						[	
 * 							'code' (WS_RESPONSE_MESSAGE_CODE) 		: <c�digo de retorno>, 
 * 							'value' (WS_RESPONSE_MESSAGE_VALUE) 	: <mensagem>	
 * 						]
 * 
 * 						o <c�digo de retorno> informa se houve um erro geral WS_ERRORCODE_GENERAL = 0, ou retorna
 * 						o c�digo de retorno da funcao 'fncode' (que pode indicar sucesso ou erro)
 * 
 * 						a <mensagem> pode ser desde string simples, numeros ou at� mesmo outro array contendo dados como resposta> 
 * 	
 * Quando 'success' = 0, ou seja, Erro, ent�o o <c�digo de retorno> deve informar qual c�digo de erro e
 * a <mensagem> pode trazer mais detalhes sobre o erro.
 * 
 * Para 'success' = 1,  o <c�digo de retorno> deve informar qual resultado do processamento da fun��o e
 * a <mensagem> pode trazer mais detalhes da resposta procesada.
 * 
 * Para facilitar a integra��o de novos m�dulos e fun��es ao WebService, cada resposta de fun��o deve possuir a estrutura:
 * 	'fnresult' 	(WS_FUNC_RESULT) 	TRUE ou FALSE de fun��o foi processada com sucesso
 * 	'fncode'	(WS_FUNC_CODE)	Codigo de retorno da fun��o
 * 	'fnresp'	(WS_FUNC_RESPONSE)	Mensagem de resposta
 * 
 * 
 * ESTRUTURA DE RESPOSTA:
 * 
 * WS_MOD 				'mod' : < identificacao do modulo >,
 * 
 * WS_FUNC 				'func' : < identificacao da funcao do modulo >,
 * 
 * WS_RESPONSE_CODE			'success' : < 0 (WS_RESPONSE_ERROR) se erro de requisicao ou 1 (WS_RESPONSE_SUCCESS) se funcao processada >,
 * 
 * WS_RESPONSE_MESSAGE		'response' : [ 
 * 										'code' : < codigo de retorno 0 (WS_ERRORCODE_GENERAL) se WS_RESPONSE_CODE = WS_RESPONSE_ERROR, ou
 * 										  		  se WS_RESPONSE_CODE = WS_RESPONSE_SUCCESS codigo de retorno da funcao 'fncode' e 
 * 										  		  geralmente 'fncode' eh 0 se funcao executada com sucesso e maior que 0 se houve algum erro >,
 * 
 * 										'value' : < mensagem, que pode ser uma simples string, numeros ou ate mesmo listas e outros arrays >
 * 									]
 * 
 * 
 **/

/*
 * Registro de POST apenas para DEBUG. 
 *

file_put_contents("Log.txt", PHP_EOL . "Nova Requisicao", FILE_APPEND);

  file_put_contents("Log.txt", PHP_EOL . "-> POST", FILE_APPEND); 
  
  if (empty($_POST)) { 
  		file_put_contents("Log.txt", PHP_EOL . "	-> is Empty", FILE_APPEND); 
  } else { 
  		$INPUTS = $_POST; 
  		foreach ($INPUTS as $VAR => $VALUE) { 
  			file_put_contents("Log.txt", PHP_EOL . "	-> " . $VAR . " : " .$VALUE, FILE_APPEND); 
  		} 
  } 
  
  file_put_contents("Log.txt", PHP_EOL . "-> GET", FILE_APPEND);
  
  if (empty($_GET)) {
  	file_put_contents("Log.txt", PHP_EOL . "	-> is Empty", FILE_APPEND);
  } else {
  	$INPUTS = $_GET;
  	foreach ($INPUTS as $VAR => $VALUE) {
  		file_put_contents("Log.txt", PHP_EOL . "	-> " . $VAR . " : " .$VALUE, FILE_APPEND);
  	}
  }
  
 /* DEBUG
 */


/*
 *	Include principal.
 */

require ("include/GlobalDefs.php");

header('Content-type: application/json');

/*
 * Montar estrutura basica para resposta do Webservice
 */
$response = array (
		WS_MOD => NULL,
		WS_FUNC => NULL,
		WS_RESPONSE_CODE => NULL,
		WS_RESPONSE_MESSAGE => array( WS_RESPONSE_MESSAGE_CODE => NULL, WS_RESPONSE_MESSAGE_VALUE => NULL )
);


/*
 * Tratamento da requisi��o
 */
if ( !empty( $_POST ) ) {
	
	//print_r('[POST]');

	// POST
	
	/*
	 * Encontrar entrada POST do m�dulo WS_MOD
	*/
	if (! isset ( $_POST [WS_MOD] ) || $_POST [WS_MOD] == '') {
	
		//print_r('[MOD MISSING]');

		$response [WS_RESPONSE_CODE] = WS_RESPONSE_ERROR;
		$response [WS_RESPONSE_MESSAGE][WS_RESPONSE_MESSAGE_CODE] = WS_ERRORCODE_GENERAL;
		$response [WS_RESPONSE_MESSAGE][WS_RESPONSE_MESSAGE_VALUE] = WS_ERRORMSG_MOD_NOT_FOUND;
			
		die ( json_encode ( $response ) );
	}
	else {
		
		//print_r('[MOD OK]');

		$response [WS_MOD] = $_POST [WS_MOD];

	}
	
	/*
	 * Encontrar entrada POST da fun��o WS_FUNC
	*/	
	if (! isset ( $_POST [WS_FUNC] ) || $_POST [WS_FUNC] == '') {
	
		//print_r('[FUNC MISSING]');

		$response [WS_RESPONSE_CODE] = WS_RESPONSE_ERROR;
		$response [WS_RESPONSE_MESSAGE][WS_RESPONSE_MESSAGE_CODE] = WS_ERRORCODE_GENERAL;
		$response [WS_RESPONSE_MESSAGE][WS_RESPONSE_MESSAGE_VALUE] = WS_ERRORMSG_FUNC_NOT_FOUND;
		
		die ( json_encode ( $response ) );
	}
	else {

		//print_r('[FUNC OK]');

		$response [WS_FUNC] = $_POST [WS_FUNC];
	
	}
	
} else if ( !empty( $_GET ) ) {
	
	//print_r('[GET]');

	// GET
	
	/*
	 * Encontrar entrada GET do m�dulo WS_MOD
	*/
	if (! isset ( $_GET [WS_MOD] ) || $_GET [WS_MOD] == '') {
	
		//print_r('[MOD MISSING]');

		$response [WS_RESPONSE_CODE] = WS_RESPONSE_ERROR;
		$response [WS_RESPONSE_MESSAGE][WS_RESPONSE_MESSAGE_CODE] = WS_ERRORCODE_GENERAL;
		$response [WS_RESPONSE_MESSAGE][WS_RESPONSE_MESSAGE_VALUE] = WS_ERRORMSG_MOD_NOT_FOUND;
	
		die ( json_encode ( $response ) );
	}
	else {

		//print_r('[MOD OK]');
	
		$response [WS_MOD] = $_GET [WS_MOD];

	}
	
	/*
	 * Encontrar entrada GET da fun��o WS_FUNC
	*/
	if (! isset ( $_GET [WS_FUNC] ) || $_GET [WS_FUNC] == '') {
	
		print_r('[FUNC MISSING]');

		$response [WS_RESPONSE_CODE] = WS_RESPONSE_ERROR;
		$response [WS_RESPONSE_MESSAGE][WS_RESPONSE_MESSAGE_CODE] = WS_ERRORCODE_GENERAL;
		$response [WS_RESPONSE_MESSAGE][WS_RESPONSE_MESSAGE_VALUE] = WS_ERRORMSG_FUNC_NOT_FOUND;
	
		die ( json_encode ( $response ) );
	}
	else {

		//print_r('[FUNC OK]');

		$response [WS_FUNC] = $_GET [WS_FUNC];
	
	}
	
} else {
	
	$response [WS_RESPONSE_CODE] = WS_RESPONSE_ERROR;
	$response [WS_RESPONSE_MESSAGE][WS_RESPONSE_MESSAGE_CODE] = WS_ERRORCODE_GENERAL;
	$response [WS_RESPONSE_MESSAGE][WS_RESPONSE_MESSAGE_VALUE] = WS_ERRORMSG_REQUEST_ERROR;
	
	die ( json_encode ( $response ) );
}


/*
 * Include dos m�dulos.
 */

require ("modules/smi/smi.php");

/*
 * Executar chamadas dos m�dulos
 */

// Obter modulo e fun��o (POST ou GET)
if ( !empty($_POST) ) {

	$mod = $_POST [WS_MOD];
	$func = $_POST [WS_FUNC];
		
} else {
	
	$mod = $_GET [WS_MOD];
	$func = $_GET [WS_FUNC];
	
}

// Registrar MOD e FUNC na resposta
$response [WS_MOD] = $mod;
$response [WS_FUNC] = $func;

//print_r('[PROCESSING REQUEST mod=' . $mod . ' func=' . $func . ']');


/*
 * M�DULO ACESSO
 */

if ($mod == MOD_SMI) {
	
	//print_r('[SMI]');

	// Instanciar objeto SMI
	$my_smi = new SMI();
	
	// Processar SMI
	if ($func == FUNC_SMI_GET_METERING) {

		//print_r('[GET METERING]');
		
		/*
		 * 	Get Metering
		 * 
		 */
		
		// Obter parametros pelo POST
		if ( ! isset ( $_GET [PARAM_SMI_METER_ADDRESS] ) || $_GET [PARAM_SMI_METER_ADDRESS] == '' ) {

			//print_r('[PARAM ERROR]');

			$response [WS_RESPONSE_CODE] = WS_RESPONSE_ERROR;
			$response [WS_RESPONSE_MESSAGE][WS_RESPONSE_MESSAGE_CODE] = WS_ERRORCODE_GENERAL;
			$response [WS_RESPONSE_MESSAGE][WS_RESPONSE_MESSAGE_VALUE] = WS_ERRORMSG_PARAM_ERROR;

		} else {

			$meter_addr = $_GET [PARAM_SMI_METER_ADDRESS];

			//print_r('[PARAM OK meter addr=' . $meter_addr . ']');

			$rc = $my_smi->getMetering ( $meter_addr );
			//print_r($rc);

			if($rc[WS_FUNC_RESULT] == TRUE) {


				$response [WS_RESPONSE_CODE] = WS_RESPONSE_SUCCESS;

			} else {

				$response [WS_RESPONSE_CODE] = WS_RESPONSE_ERROR;

			}

			$response [WS_RESPONSE_MESSAGE][WS_RESPONSE_MESSAGE_CODE] = $rc[WS_FUNC_CODE];
			$response [WS_RESPONSE_MESSAGE][WS_RESPONSE_MESSAGE_VALUE] = $rc[WS_FUNC_RESPONSE];
			
		}
		
	} else {
		
		/*
		 * Fun��o inv�lida
		 */

		//print_r('[INVALID FUNC]');

		$response [WS_RESPONSE_CODE] = WS_RESPONSE_ERROR;
		$response [WS_RESPONSE_MESSAGE][WS_RESPONSE_MESSAGE_CODE] = WS_ERRORCODE_GENERAL;
		$response [WS_RESPONSE_MESSAGE][WS_RESPONSE_MESSAGE_VALUE] = WS_ERRORMSG_INVALID_FUNC;
	
	}

} 

/*
 * M�DULO INVALIDO
*/

else {
	
	//print_r('[INVALID MOD]');

	$response [WS_RESPONSE_CODE] = WS_RESPONSE_ERROR;
	$response [WS_RESPONSE_MESSAGE][WS_RESPONSE_MESSAGE_CODE] = WS_ERRORCODE_GENERAL;
	$response [WS_RESPONSE_MESSAGE][WS_RESPONSE_MESSAGE_VALUE] = WS_ERRORMSG_INVALID_MOD;
	
}

/*
 * Enviar resposta.
 */

if ( $response != NULL) {

	echo json_encode ( $response );
	
}

?>
