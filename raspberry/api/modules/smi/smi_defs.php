<?php

// Identificação do modulo
define("MOD_SMI", 'smi');

// Metodos de acesso
define("FUNC_SMI_GET_METERING", 'getmetering');

// Parametros de entrada
define("PARAM_SMI_METER_ADDRESS", 'metaddr');

// Codigos de retorno
define("RETURN_SMI_GETMETERING_OK", 0);
define("RETURN_SMI_GETMETERING_EMPTY", 1);

// Mensagens de retorno
define("RETURN_SMI_MESSAGE", 'Message');

define("RETURN_SMI_TIMESTAMP", 'TimeStamp');
define("RETURN_SMI_METERING", 'Metering');

define("RETURN_SMI_VOLTAGE", 'Voltage');
define("RETURN_SMI_CURRENT", 'Current');
define("RETURN_SMI_PF", 'pf');
define("RETURN_SMI_WH", 'Wh');
define("RETURN_SMI_VARH", 'varh');

?>
