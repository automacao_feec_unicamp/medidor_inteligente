<?php

 require_once 'smi_defs.php';

 class SMI {

    private $db;

    function __construct() {

	//print_r('[SMI construct]');

	$this->db = new SQLite3('/var/www/html/database/smi.db');

    }

    function __destruct() {

	//print_r('[SMI destruct]');

	$this->db->close();

    }

    /*
     * \brief	Ler medi��es
    *
    * \param	$aAddress	Endereco do medidor
    *
    * \return	WS_FUNC_RESULT	TRUE se fun��o executada com sucesso
    * \return	WS_FUNC_CODE		0	Registro efetuado com sucesso (RETURN_DEVICE_OK)
    * 					1	Erro efetuando registro (RETURN_DEVICE_REGISTER_ERROR)
    * 					2	Dispositivo ja existe na rede (RETURN_DEVICE_REGISTER_DEVICE_EXISTS)
    * 
    */

    function getMetering( $aMeterAddr  ) {

	$ret = array();

	try {

		# SELECT Meter.Address, Metering.TimeStamp, MeteringData.Data, MeteringData.Value
		# FROM (Meter INNER JOIN Metering ON Meter.Id = Metering.MeterId) INNER JOIN MeteringData ON Metering.Id = MeteringData.MeteringId;

		//print_r('[GET METERING QUERY]');

		$result = $this->db->query( "SELECT Meter.Address, Metering.TimeStamp, MeteringData.Data, MeteringData.Value FROM 
						(Meter INNER JOIN Metering ON Meter.Id = Metering.MeterId) 
						INNER JOIN MeteringData ON Metering.Id = MeteringData.MeteringId 
						WHERE Meter.Address = '$aMeterAddr' 
						ORDER BY Metering.TimeStamp DESC;" );

		$no_of_rows = 0;

		/**
		 * $Metering = array (
	 	 * 	'TimeStamp' => <YYYY-MM-DD HH:MM:SS>
		 *	'Metering' => array( 'Voltage', 'Current', 'pf', 'Wh', 'varh' )
		 * );
		 */

		$Metering = array();
		$MeteringTimeStamp = -1;
		$MeteringData = array( RETURN_SMI_VOLTAGE => NULL, RETURN_SMI_CURRENT => NULL, RETURN_SMI_PF => NULL, RETURN_SMI_WH => NULL, RETURN_SMI_VARH => NULL );

		while( $row = $result->fetchArray() ) {

				//print "[Address " . $row[ 'Address' ] . " TimeStamp " . $row[ 'TimeStamp' ] . " Data " . $row[ 'Data' ] . " " . $row[ 'Value' ] .  "]\n";
				
				if ( $MeteringTimeStamp == -1 ) {

					$MeteringTimeStamp = $row[ 'TimeStamp' ];

					//print_r('[timestamp INIT ' . $MeteringTimeStamp . ']');

				}

				if ( $MeteringTimeStamp != $row['TimeStamp'] ) {

					//print_r('[CLOSING timestamp ' . $MeteringTimeStamp . ']');

					$MeteringItem = array();
					$MeteringItem[ RETURN_SMI_TIMESTAMP ] = $MeteringTimeStamp;
					$MeteringItem[ RETURN_SMI_METERING ] = $MeteringData;

					array_push($Metering, $MeteringItem);

					//$Metering[ $MeteringTimeStamp ] = $MeteringData;

					$MeteringTimeStamp = $row['TimeStamp'];

					$no_of_rows = $no_of_rows + 1;

					//print_r('[OPENNIG timestamp ' . $MeteringTimeStamp . ']');

					$MeteringData = array( RETURN_SMI_VOLTAGE => NULL, RETURN_SMI_CURRENT => NULL, RETURN_SMI_PF => NULL, RETURN_SMI_WH => NULL, RETURN_SMI_VARH => NULL );

				}
		
				# Data code 1 = voltage (V)
				# Data code 2 = current (A)
				# Data code 3 = power factor
				# Data code 4 = tot wh (wh)
				# Data code 5 = tot varh (varh)

				if( $row[ 'Data' ] == 1 ) {

					$MeteringData[ RETURN_SMI_VOLTAGE ] = $row[ 'Value' ];

					//print_r('[Voltage ' . $MeteringData[ RETURN_SMI_VOLTAGE ] . ']');

				}

				# Data 2 -> Current
				if( $row[ 'Data' ] == 2 ) {

					$MeteringData[ RETURN_SMI_CURRENT ] = $row[ 'Value' ];

					//print_r('[Current ' . $MeteringData[ RETURN_SMI_CURRENT ] . ']');
			
				}

				# Data 3 -> 
				if( $row[ 'Data' ] == 3 ) {

					$MeteringData[ RETURN_SMI_PF ] = $row[ 'Value' ];

					//print_r('[pf ' . $MeteringData[ RETURN_SMI_PF ] . ']');

				}

				# Data 4 -> Wh
				if( $row[ 'Data' ] == 4 ) {

					$MeteringData[ RETURN_SMI_WH ] = $row[ 'Value' ];

					//print_r('[Wh ' . $MeteringData[ RETURN_SMI_WH ] . ']');

				}

				# Data 5 -> varh
				if( $row[ 'Data' ] == 5 ) {

					$MeteringData[ RETURN_SMI_VARH ] = $row[ 'Value' ];

					//print_r('[varh ' . $MeteringData[ RETURN_SMI_VARH ] . ']');

				}

		}
			
		$ret[WS_FUNC_RESPONSE] = array(RETURN_SMI_MESSAGE => $Metering);
		
		//print_r('#### ' . $no_of_rows . ' #####');

		if($no_of_rows > 0) {

			$ret[WS_FUNC_CODE] = RETURN_SMI_GETMETERING_OK;

		} 
		else {

			$ret[WS_FUNC_CODE] = RETURN_SMI_GETMETERING_EMPTY;
			$ret[WS_FUNC_RESPONSE] = array(RETURN_SMI_MESSAGE => 'Registro de medicao nao disponivel');			

		}

		// Fun��o foi executada com sucesso (acesso ao banco de dados)
		$rc[WS_FUNC_RESULT] = TRUE;

     }
     catch(Exception $e) {
	
		// Erro executando fun��o (acesso ao banco de dados)
		$ret[WS_FUNC_RESULT] = FALSE;

     }

     return $ret;

    }

 }

?>
