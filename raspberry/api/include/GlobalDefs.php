<?php

/**
 * Defini��es gerais
 */

// Defini��o das entradas Webservice
define("WS_MOD", 'mod');
define("WS_FUNC", 'func');

// Defini��o para respostas das fun��es dos m�dulos
define("WS_FUNC_RESULT", 'fnresult'); 	// TRUE ou FALSE se fun��o foi executada com sucesso
define("WS_FUNC_CODE", 'fncode');		// Reservado para definir codigos de retorno
define("WS_FUNC_RESPONSE", 'fnresp');	// Reservado para definir mensagens ou estruturas de dado como resposta (arrays)

// Defini��o dos c�digos de resposta
define("WS_RESPONSE_CODE", 'success');

define("WS_RESPONSE_SUCCESS", 1);
define("WS_RESPONSE_ERROR", 0);

// Em caso de sucesso  ("WS_RESPONSE_CODE" = "WS_RESPONSE_SUCCESS" = 1)
// e junto com a resposta 'pode' ir na mensagem "WS_RESPONSE_MESSAGE" o c�digo de retorno
// e mensagem que � definido por cada m�dulo
// Quando houver erro ("WS_RESPONSE_CODE" = "WS_RESPONSE_ERROR" = 0)
// ent�o junto com a resposta 'pode' ir na mensagem "WS_RESPONSE_MESSAGE" o c�digo de 
// erro e mensagem
define("WS_RESPONSE_MESSAGE", 'response');

define("WS_RESPONSE_MESSAGE_CODE", 'code');
define("WS_RESPONSE_MESSAGE_VALUE", 'value');

// Defini��es de erro geral (reservado codigo de erro 0)
define("WS_ERRORCODE_GENERAL", 0);

define("WS_ERRORMSG_REQUEST_ERROR", 'Error');

define("WS_ERRORMSG_MOD_NOT_FOUND", 'Module is missing');
define("WS_ERRORMSG_INVALID_MOD", 'Module not supported');

define("WS_ERRORMSG_FUNC_NOT_FOUND", 'Function is missing');
define("WS_ERRORMSG_INVALID_FUNC", 'Function not supported');

define("WS_ERRORMSG_PARAM_ERROR", 'Parameters error');

?>
