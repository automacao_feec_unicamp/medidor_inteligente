INSERT INTO Meter(Id, Name, Address) VALUES
(1, 'Meter A', '1'),
(2, 'Meter B', '2'),
(3, 'Meter C', '3');

INSERT INTO Metering(MeterId) VALUES
(1);

INSERT INTO MeteringData(MeteringId, Data, Value) VALUES
(1, 1, 110.5),
(1, 2, 5.3);

