import time
import os
from threading import Thread
import serial
import struct
from time import gmtime, strftime


class Meter(Thread):

	_address = None
	_commport = '/dev/ttyAMA0'
	_metering = [] # timestamp, voltage, current, pf, wh, varh
	_meteringReady = False
	_soframe = '\x55'
	_eoframe = '\xAA'
	# TODO implement CRC-16 or Cheksum

	def __init__ (self, address = None):
		
		Thread.__init__(self)
		self._address = address
		
	def run(self):

		print '## Starting Meter at Address ' + self._address

		self._commport = serial.Serial(self._commport, baudrate = 9600, timeout = 5.0, xonxoff=False, rtscts=False, dsrdtr=False)

		while True:
			
			time.sleep(1)
			
			rxdata = []

			voltage = 0.0
			current = 0.0
			wh = 0.0
			varh = 0.0
			fp = 0.0
						
			print '## Reading Meter ' + self._address
		
			# Try to read the Start of Frame at first		
			rxdata = self._commport.read(1)
	
			if len(rxdata):
				
				if rxdata == self._soframe:

					rxdata = rxdata + self._commport.read(13)

					if len(rxdata) == 14:
			
						print '## Meter ' + self._address + ' RX < ' + rxdata.encode('hex') + '\n'
				
						# End of Frame check
						if rxdata[13] == self._eoframe:

							timestamp = strftime("%Y-%m-%d %H:%M:%S", gmtime())
	
							voltage, = struct.unpack('>f', rxdata[1:5])
							current, = struct.unpack('>f', rxdata[5:9])
							wh, = struct.unpack('>f', rxdata[9:13])
				
							print "## Voltage %f   Current %f   Wh %f \n  " % (voltage, current, wh)
				
							self._metering = self._metering + [[ timestamp, voltage, current, 0.0, wh, 0.0 ]]
							self._meteringReady = True

						else:

							print '## print '## Invalid End of Frame\n'\n'

					else:
			
						print '## Frame Size Error\n'

				else:
				
					print '## Invalid Start of Frame\n'

			else:

				print '## Meter ' + self._address + ' RX < TIMEOUT\n'

			# Data generation [simulation]
			#self._metering = self._metering + [[ 0.0, 110.0, 1.2, 0.95, 1.1, 0.03 ]]
			#self._meteringReady = True

			#print self._metering

	def meteringAvailable(self):	
				
		return self._meteringReady

	def meteringGet(self):
		
		meteringNow = []
		meteringNow = self._metering

		self._meteringReady = False
		self._metering = []
				
		return meteringNow
