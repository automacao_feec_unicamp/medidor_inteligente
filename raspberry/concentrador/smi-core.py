#!/usr/bin/python

import os
import sqlite3
import time
import signal
from meter import Meter


def main():
	
	print "\nStarting SMI - Core...\n"
	
	os.system('systemctl stop serial-getty@ttyAMA0.service')
	time.sleep(3)
	
	DbConnection = sqlite3.connect('/var/www/html/database/smi.db')
	DbCursor = DbConnection.cursor()

	print "\nLoading Meters...\n"

	Meters = []
	DbCursor.execute('SELECT Id, Name, Address FROM Meter')	
	for Row in DbCursor:
		print 'Meter Id [%d] Name [%s] Address[%s]' % (Row[0], Row[1], Row[2])
		Meters = Meters + [[ Row[0], Row[1], Row[2], Meter(Row[2]) ]]
	
	#print Meters
	
	try:
		
		print "\nStarting Meters...\n"

		for MeterIdx in Meters:
			print '[%s] Init' % (MeterIdx[1])
			MeterIdx[3].start()

		while True:

			time.sleep(5)
			metering = []
			metering_lastid = 0
			sql = ''

			for MeterIdx in Meters:

				if (MeterIdx[3].meteringAvailable() == True):

					print '[%s] Metering available' % (MeterIdx[1])

					metering = MeterIdx[3].meteringGet()
					print metering

					for metering_row in metering:
					
						# Store metering in the database

						# Create metering record
						#sql = "INSERT INTO Metering (MeterId) VALUES (%i);" % (MeterIdx[0])
						sql = "INSERT INTO Metering (MeterId, TimeStamp) VALUES (%i, '%s');" % (MeterIdx[0], metering_row[0])
						#print sql
						DbCursor.execute(sql)
						DbConnection.commit()
					
						# Get last Metering Id
						sql = "SELECT * FROM SQLITE_SEQUENCE WHERE name = 'Metering';";
						DbCursor.execute(sql)
					
						for row in DbCursor:
							#print row[1]
							metering_lastid = row[1]

						print '[%s] Metering Id %d' % (MeterIdx[1], metering_lastid)
						
						# Insert Data code 1 = voltage (V)
						#        Data code 2 = current (A)
						#        Data code 3 = power factor
						#        Data code 4 = tot wh (wh)
						#        Data code 5 = tot varh (varh)

						sql = "INSERT INTO MeteringData (MeteringId, Data, Value) VALUES "
						sql = sql + "(%i, %i, %f), " % (metering_lastid, 1, metering_row[1]) # metering_row[1] = voltage (ref. meter.py)
						sql = sql + "(%i, %i, %f), " % (metering_lastid, 2, metering_row[2]) # metering_row[2] = current (ref. meter.py)
						sql = sql + "(%i, %i, %f), " % (metering_lastid, 3, metering_row[3]) # metering_row[3] = pf (ref. meter.py)
						sql = sql + "(%i, %i, %f), " % (metering_lastid, 4, metering_row[4]) # metering_row[4] = tot wh (ref. meter.py)
						sql = sql + "(%i, %i, %f);" % (metering_lastid, 5, metering_row[5]) # metering_row[5] = tot varh (ref. meter.py)

						print sql

						DbCursor.execute(sql)
						DbConnection.commit()

						metering = []

	except KeyboardInterrupt:
		
		print "Stopping SMI...\n"

		for MeterIdx in Meters:
			print 'Stopping [%s]' % (MeterIdx[1])
			if MeterIdx[3].isAlive(): MeterIdx[3].kill_received = True
		
		pid = os.getpid()
		os.kill(pid, signal.SIGKILL)

		DbCursor.close()
		DbConnection.close()
		

if __name__=="__main__":
	
	main()
